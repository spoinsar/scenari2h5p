#!/usr/bin/python3
# encoding: utf-8




import sys
import os
import shutil
import argparse
import unicodedata
import re
import traceback
import json

from zipfile import ZipFile

from lxml import etree
#from macpath import dirname

#from pydev import pydevd

def escape_text(e):     # for mathtex latex syntax and also for python escaping
    #text = text.replace('\\','\\textbackslash') # backslash come first because other commands write some more
    tex_elements = e.findall("{" + ns['sc'] +"}textLeaf")
    if tex_elements:
        for tex_element in tex_elements:
            if tex_element:
                tex_element.text = '\( ' + tex_element.text + ' \)'
                #print(tex_element.text)

    list_element = e.findall("{" + ns["sc"] + "}itemizedList")
    if list_element:
        list_element[0][0].text = "<ul>" + list_element[0][0].text
        for item in list_element:
            item[0].text = "<li>" + item[0].text + "</li>"
        list_element[len(list_element)-1][0].text = list_element[len(list_element)-1][0].text + "</ul>"


    text = e.xpath("string()")
    #print(question[1].xpath("string()"))

    print(text)
    #if e.attrib
        #text = text.replace('{' + ns[sc] + '}textLeaf role="mathtex">', '\\textbackslash\(')
        #text = text.replace('</sc:textLeaf role="mathtex">', '\\textbackslash\(')
    text = text.replace('\\','\\\\') # backslash has to be doubled to be understood as a backslash for latex
        # text = text.replace('~','\\textasciitilde')
        # text = text.replace('^','\\textasciicircum')
        # text = text.replace('&','\\&')
        # text = text.replace('%','\\%')
        # text = text.replace('$','\\$')
        # text = text.replace('#','\\#')
        # text = text.replace('_','\\_')
        # text = text.replace('{','\\{')
        # text = text.replace('}','\\}')
    print(text)
    e.text = text

def single_answer(structure, i, e):
    structure["questions"][i]["params"]["behaviour"]["type"] = "auto"

    sol = int(e.find(".//{http://www.utc.fr/ics/scenari/v3/core}solution").attrib['choice'])
    print(sol)

    choices = e.findall('.//{' + ns["sc"] + '}choice', ns)
    print(choices[0].attrib)
    # find answers and their corectness


    for k in range(len(choices)):
        escape_text(choices[k][0][0][0])
        structure["questions"][i]["params"]["answers"].append({
            "correct": False,
            "text": "<div>"+choices[k][0][0][0].text+"</div>\n",
            "tipsAndFeedback": {
                "tip": "",
                "chosenFeedback": "",
                "notChosenFeedback": ""
            }
        })
        if k == sol-1:
            structure["questions"][i]["params"]["answers"][k]["correct"] = True

    # except:
    #     print("nothing")


def multiple_answer(structure, i, e):
    structure["questions"][i]["params"]["behaviour"]["type"] = "checkboxes"

    choices = e.findall('.//{' + ns["sc"] + '}choice', ns)
    choices_bool = []
    print(choices[0].attrib)
    true_nb = 0
    # find answers and their corectness
    for k in range(len(choices)):
        if choices[k].attrib == {'solution': 'checked'}:
            choices_bool.append(True)
            true_nb += 1
        else:
            choices_bool.append(False)

    for k in range(len(choices)):
        escape_text(choices[k][0][0][0])
        structure["questions"][i]["params"]["answers"].append({
            "correct": choices_bool[k],
            "text": "<div>"+choices[k][0][0][0].text+"</div>\n",
            "tipsAndFeedback": {
                "tip": "",
                "chosenFeedback": "",
                "notChosenFeedback": ""
            }
        })

def readquiz():
    nodelet = ""
    for file in os.listdir("./tmp"):
        if file.endswith(".nodelet"):
            nodelet = os.path.join("./tmp", file)

    tree = etree.parse(nodelet, parser)

    if (not tree):
        print('Error: XML from "'+effectivefile+'" not loaded correctly (parse)')
        #return
    root = tree.getroot()
    if (not tree):
        print('Error: XML from "'+effectivefile+'" not loaded correctly (getroot)')

    quizzes_tags = root[0].findall('.//{' + ns['sp']+ '}quiz') # list of the quiz file

    quizzes = []

    for quiz_tag in quizzes_tags:
        quizzes.append(quiz_tag.attrib["{" + ns["sc"] + "}refUri"])

    return(quizzes)

def convert_group(structure, quizzes):
    for i, quiz in enumerate(quizzes):
        quiz = quiz.replace('&', 'tmp/&')

        structure["questions"].append({
            "params": {
                "answers": [ #put answers here
                ],
                "UI": {
                  "showSolutionButton": "Voir la solution",
                  "tryAgainButton": "Réessayer",
                  "checkAnswerButton": "Vérifier",
                  "tipsLabel": "Voir le conseil",
                  "scoreBarLabel": "Vous avez :num sur :total points",
                  "tipAvailable": "Conseil disponible",
                  "feedbackAvailable": "Commentaires disponibles",
                  "readFeedback": "Lire les commentaires",
                  "wrongAnswer": "Réponse fausse",
                  "correctAnswer": "Réponse correcte",
                  "shouldCheck": "Aurait dû être cochée",
                  "shouldNotCheck": "N'aurait pas dû être cochée",
                  "noInput": "Veuillez répondre avant de voir la solution"
                },
                "question": "", #question here
                "behaviour": {
                  "enableRetry": True,      # changer ici pour les exams
                  "enableSolutionsButton": True,    # changer ici pour les exams
                  "singlePoint": False,
                  "randomAnswers": True,
                  "showSolutionsRequiresInput": True,
                  "type": "auto",  # here "checkboxes" means this and "auto" can be used for single answers
                  "confirmCheckDialog": False,
                  "confirmRetryDialog": False,
                  "autoCheck": False,
                  "passPercentage": 100,
                  "showScorePoints": True,
                  "enableCheckButton": True
                },
                # media here
                "confirmCheck": {
                  "header": "Fini ?",
                  "body": "Êtes-vous sûr de bien avoir fini votre réponse ?",
                  "cancelLabel": "Annuler",
                  "confirmLabel": "Finir"
                },
                "confirmRetry": {
                  "header": "Réessayer ?",
                  "body": "Êtes-vous sûr de vouloir réessayer ?",
                  "cancelLabel": "Annuler",
                  "confirmLabel": "Oui"
                },
                "overallFeedback": [
                  {
                    "from": 0,
                    "to": 0,
                    "feedback": "Faux !"
                  },
                  {
                    "from": 1,
                    "to": 99,
                    "feedback": "Presque !"
                  },
                  {
                    "from": 100,
                    "to": 100,
                    "feedback": "Correct !"
                  }
                ]
              },
              "library": "H5P.MultiChoice 1.14",
              "subContentId": "bd03477a-90a1-486d-890b-0657d6e80ffd",
              "metadata": {
                "title": "", #question here
                "license": "U",
                "contentType": "Multiple Choice"
             }
        })

        tree = etree.parse(quiz, parser)


        if (not tree):
            print('Error: XML from "'+effectivefile+'" not loaded correctly (parse)')
            #return
        root = tree.getroot()
        if (not tree):
            print('Error: XML from "'+effectivefile+'" not loaded correctly (getroot)')
            #return
        #print(root[0].findall('.//{http://www.utc.fr/ics/scenari/v3/primitive}keywd')[2].text)
        #print(root[0].findall('.//{sc}choice',ns))

        #find question
        question = root[0].findall('.//{' + ns['sc']+ '}question')[0][0][0][0] # list of paragraph tags



        # add title

        structure["questions"][i]["metadata"]["title"] = question[0].text

        for paragraph in question:
            escape_text(paragraph)
            structure["questions"][i]["params"]["question"] += "<p>" + paragraph.text + "</p>\n"

        if (root[0].tag == "{utc.fr:ics/opale3}mcqSur"):
            single_answer(structure, i, root[0])

        else:
            multiple_answer(structure, i, root[0])

        i = 0
        for fileTag in root[0].findall('.//{' + ns["sp"] + '}res', ns):
            print("a")
            file = fileTag.attrib['{' + ns["sc"] + '}refUri']
            pathToSrcQuestions = os.path.dirname("./" + quiz) + "/"
            pathToDstQuestions = "./question-test/content/images/"
            ext = str(os.path.splitext(file)[1])
            print(file)
            fileName = os.path.basename(file)
            #print(fileName)
            #print(ext)
            src = os.path.abspath(pathToSrcQuestions + file + "/" + fileName)
            dst = os.path.abspath(pathToDstQuestions) + "/" + str(i) + ext

            print("src", src)
            print("dst", dst)
            shutil.copyfile(src, dst)
            if ext == ".png":
                mime = "image/png"
            elif ext == ".jpg" or ext == ".jpeg":
                mime = "image/jpeg"
            else:
                mime = ""
            structure["questions"][i]["params"]["media"] = {
                "type": {
                    "library": "H5P.Image 1.1",
                    "params": {
                      "contentName": "Image",
                      "alt": "Image",
                      "file": {
                        "path": 'images/' + str(i) + ext,
                        "mime": mime,
                        #"width": 1588,
                        #"height": 458,
                        "copyright": {
                          "license": "U"
                        }
                      }
                    },
                    #"subContentId": "5d791eac-5e66-472a-a7e7-e13f132f7873",
                    # "metadata": {
                    #   "title": "Image",
                    #   "authors": [
                    #     {
                    #       "name": "Gloria Cabada-Leman",
                    #       "role": "Author"
                    #     }
                    #   ],
                    #   "source": "https://www.flickr.com/photos/67238971@N04/6299792743/in/photolist-aAG5Ug-dd9xLw-bVqsiX-cJksCw-6kcNtS-5jEMb2-5aJC9S-6Cruws-9LZATE-5ikCY1-9LZAFL-8AxwwV-9XwKxE-fpjPyz-8d1GNx-9LZABd-t28p4-fpjQ3V-bzT5MZ-dVHye1-eeHhvG-ccMGNJ-ccMGPW-f6xAxP-oZzvWs-aed6pq-5srZfB-",
                    #   "license": "CC BY-ND",
                    #   "licenseVersion": "4.0",
                    #   "contentType": "Image"
                    # }
                },
                "disableImageZooming": True
            }

        explanationTag = root[0].find("{" + ns["sc"] +"}globalExplanation")
        if explanationTag:
            for paragraph in explanationTag[0][0][0]:
                escape_text(paragraph)
                structure["questions"][i]["params"]["overallFeedback"][2]["feedback"] += " " + paragraph.text






#pydevd.settrace("localhost", port=5678)
parser = etree.XMLParser(remove_blank_text=True, remove_comments=True, encoding="utf-8")


#parser = argparse.ArgumentParser(description='Convert from Opale (xml) to Auto Multiple Choices (LaTeX)')
#
# parser.add_argument('inputfile', help='Root Scenari item - AMC export (best), module, quiz... as a filesystem path')
# options = parser.parse_args()

ns={
    "sc":"http://www.utc.fr/ics/scenari/v3/core",
    "op":"utc.fr:ics/opale3",
    "sp":"http://www.utc.fr/ics/scenari/v3/primitive"
    }


# file_name = "ChimSolu_N3_2021-7-25.scar"
#
# with ZipFile(file_name) as zip:
#     zip.printdir()
#     zip.extractall("tmp")

#quizzes = readquiz()

quizzes = ["CA-2012-CH-09.quiz"]

#file = 'UBG-BAN-005.quiz'
#file = 'INL-MEI-023.quiz'
#file = 'INL-FMV-006.quiz'
#file = 'CA-2012-CH-09.quiz'
#out = []


structure = {
    "progressType": "dots",
    "passPercentage": 50,
    "questions": [

    ]

}

convert_group(structure, quizzes)


#print(structure)
with open('question-test/content/content.json', 'w') as json_file:
    json.dump(structure, json_file, indent = 4, sort_keys=True, ensure_ascii=False)

#zipper le h5p
#paramétrer les retry (poour les évaluations), tout ça, avec xargs
